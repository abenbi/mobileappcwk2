package com.example.cwk2mobapp;

import static android.provider.BaseColumns._ID ;
import static com.example.cwk2mobapp.Constants.TABLE_NAME ;
import static com.example.cwk2mobapp.Constants.NAMEOFPRODUCT;
import static com.example.cwk2mobapp.Constants.WEIGHT;
import static com.example.cwk2mobapp.Constants.PRICE;
import static com.example.cwk2mobapp.Constants.DESCRIPTION;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ingredientsData extends SQLiteOpenHelper {
    private static final String TAG = "HELP";

    private static final String DATABASE_NAME = " ingredients ";
    private static final int DATABASE_VERSION = 1;



    public ingredientsData(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createTable = " CREATE TABLE " + TABLE_NAME + "("
                + _ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NAMEOFPRODUCT + " TEXT NOT NULL,"
                + WEIGHT + " DOUBLE, "
                + PRICE + " TEXT NOT NULL, "
                + DESCRIPTION + " TEXT NOT NULL);";
        db.execSQL(createTable);
        System.out.println("CREATE THIS TABLE    "+ createTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);

}

    public Boolean saveData(String item, Double item1, double item2, String item3) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAMEOFPRODUCT, item);
        contentValues.put(WEIGHT, item1);
        contentValues.put(PRICE, item2);
        contentValues.put(DESCRIPTION, item3);


        Log.d(TAG, "ADD DATA: ADDING " + item + "to " + TABLE_NAME);
        long result = db.insert(TABLE_NAME, null, contentValues);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

}

