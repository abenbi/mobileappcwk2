package com.example.cwk2mobapp;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import org.w3c.dom.Text;

import static android.provider.BaseColumns._ID;
import static com.example.cwk2mobapp.Constants.DESCRIPTION;
import static com.example.cwk2mobapp.Constants.NAMEOFPRODUCT;
import static com.example.cwk2mobapp.Constants.PRICE;
import static com.example.cwk2mobapp.Constants.TABLE_NAME;
import static com.example.cwk2mobapp.Constants.WEIGHT;


public class Main2Activity extends AppCompatActivity {

    private Button save;
    private EditText NOP;
    private EditText Weight;
    private EditText Price;
    private EditText DescriptionOfProduct;

    private ingredientsData iData;
    private static String [] FROM = { _ID , NAMEOFPRODUCT , DESCRIPTION , PRICE};
    private static String ORDER_BY = WEIGHT + " DESC ";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

//        NOP = (EditText) findViewById(R.id.NOP);
//        Weight = (EditText) findViewById(R.id.weight);
//        Price = (EditText) findViewById(R.id.price);
//        Description = (EditText) findViewById(R.id.description);
        save = (Button) findViewById(R.id.save);
//        private ingredientsData ingredientsData;

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iData = new ingredientsData(Main2Activity.this);
                NOP = (EditText) findViewById(R.id.NOP);
                Weight = (EditText) findViewById(R.id.weight);
                Price = (EditText) findViewById(R.id.price);
                DescriptionOfProduct = (EditText) findViewById(R.id.description);
                String NameProduct = NOP.getText().toString();
                String weightP = Weight.getText().toString();
                Double WeightProduct = Double.parseDouble(weightP);
                String priceP = Price.getText().toString();
                Double PriceProduct = Double.parseDouble(priceP);
                String DescriptionP = DescriptionOfProduct.getText().toString();

                System.out.println("CHECK HEREEE   " + NameProduct + "   " + WeightProduct + "    " + PriceProduct + "   " + DescriptionP );
                addIngredients(NameProduct, WeightProduct, PriceProduct, DescriptionP);
                NOP.getText().clear();
                Weight.getText().clear();
                Price.getText().clear();
                DescriptionOfProduct.getText().clear();

                Cursor cursor = getIngredients();

//                showIngredients(cursor);
                System.out.println("WHat are the  " + getIngredients().toString());
//                 Showing an output of "android.database.sqlite.SQLiteCursor@e484a69"
            }
        });



        iData = new ingredientsData(this);
        try {
            addIngredients(" ", '9', '8', " ");
            Cursor cursor = getIngredients();
     //      showIngredients(cursor);
        } finally {
            iData.close();
        }
    }

    private void addIngredients (String name, double weights, double prices, String description){
/* Insert a new record into the Events data
source . You would do something similar
for delete and update . */

            SQLiteDatabase db = iData.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(NAMEOFPRODUCT, name);
            values.put(WEIGHT, weights);
            values.put(PRICE, prices);
            values.put(DESCRIPTION, description);
            db.insertOrThrow(TABLE_NAME, null, values);
        }

    public Cursor getIngredients () {
/* Perform a managed query . The Activity will
handle closing and re - querying the cursor
when needed . */
        SQLiteDatabase db = iData.getReadableDatabase ();
        Cursor cursor = db.query ( TABLE_NAME , FROM , null ,
                null , null , null ,
                ORDER_BY );
        return cursor;
    }

//    public void showIngredients ( Cursor cursor ) {
//// Stuff them all into a big string
//        StringBuilder builder = new StringBuilder (
//                " Saved ingredients : ");
//        while ( cursor.moveToNext ()) {
///* Could use g e t C o l u m n I n d e x O r T h r o w () to
//get indexes */
//            String id = cursor.getString (0);
//            String name = cursor.getString (1);
//            double weights = cursor.getDouble (2);
//            double price = cursor.getDouble (3);
//            String description = cursor.getString (4);
//            builder.append(id).append( ": " );
//            builder.append(name).append(": " );
//            builder.append(weights).append (": " );
//            builder.append(price).append (": " );
//     //       builder.append(description).append (": " );
//        }
//        cursor.close();
//// Display on the screen
//        TextView text = (TextView) findViewById (R.id.text);
//        text.setText(builder);
//    }







}