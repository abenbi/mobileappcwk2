package com.example.cwk2mobapp;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {

    private TextView ingredientsShow;
    private ScrollView scrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        ingredientsShow = (TextView) findViewById(R.id.ingredients2);

//        ingredientsShow.setText();






    }

    public void showIngredients ( Cursor cursor ) {
// Stuff them all into a big string
        StringBuilder builder = new StringBuilder (
                " Saved ingredients : ");
        while ( cursor.moveToNext ()) {
/* Could use g e t C o l u m n I n d e x O r T h r o w () to
get indexes */
            String id = cursor.getString (0);
            String name = cursor.getString (1);
            double weights = cursor.getDouble (2);
            double price = cursor.getDouble (3);
            String description = cursor.getString (4);
            builder.append(id).append( ": " );
            builder.append(name).append(": " );
            builder.append(weights).append (": " );
            builder.append(price).append (": " );
            //       builder.append(description).append (": " );
        }
        cursor.close();
// Display on the screen
        TextView text = (TextView) findViewById (R.id.text);
        text.setText(builder);
    }

}
